﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using VOX_WEB.Models;
using VOX_WEB.Utils;
namespace VOX_WEB.Controllers
{
    public class AccountController : Controller
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        // GET: Account
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Login(UserInput user)
        {
           
            SessionContext context = new SessionContext();
            APIExtension api = new APIExtension();
            try
            {
                if (user.password != null && user.password != "")
                {
                    string endPoint = "users/login";
                    string resAPI = await api.Post(endPoint, user);

                    var resAPIModel = JsonConvert.DeserializeObject<RestResponse>(resAPI);

                    if (resAPIModel != null)
                    {
                        var resLogin = JsonConvert.DeserializeObject<UserOutput>(resAPIModel.Data);
                        if(resLogin != null && resLogin.email != null)
                        {
                            context.SetAuthenticationToken(resLogin.email.ToString(), false, resLogin);
                            return RedirectToAction("Index", "Home");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error<Exception>(e);
            }

            TempData["Message"] = "Authentication Fail";
            return View();
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            Session.Abandon();

            HttpCookie rFormsCookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            rFormsCookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(rFormsCookie);

            HttpCookie rSessionCookie = new HttpCookie("ASP.NET_SessionId", "");
            rSessionCookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(rSessionCookie);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();

            return RedirectToAction("Login", "Account");
        }
    }
}