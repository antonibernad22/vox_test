﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using VOX_WEB.Models;
using VOX_WEB.Utils;

namespace VOX_WEB.Controllers
{
    [Authorize]

    public class OrganizerController : Controller
    {
        // GET: Organizer
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public ActionResult Index()
        {
            ViewBag.Version = Guid.NewGuid().ToString().Replace("-", "");

            return View();
        }
        public async Task<PartialViewResult> Edit(string id)
        {
            Organizer res = new Organizer();
            APIExtension api = new APIExtension();
            try
            {
                SessionContext context = new SessionContext();
                var userData = context.GetUserData();
                if (!String.IsNullOrEmpty(id))
                {
                    string endPoint = string.Format("organizers/{0}", id);
                    res = await api.Get<Organizer>(endPoint, userData.token);
                }
               
            }
            catch (Exception e)
            {
                logger.Error<Exception>(e);
            }
          
            return PartialView(res);
        }
        public async Task<JsonResult> getOrganizerAll()
        {
            OrganizerList res = new OrganizerList();

            APIExtension api = new APIExtension();

            try
            {
                SessionContext context = new SessionContext();
                var userData = context.GetUserData();

                string endPoint = "organizers?page=0&perPage=5000";
                res = await api.Get<OrganizerList>(endPoint, userData.token);
                if (res != null && res.data != null)
                {
                    res.data = res.data.OrderByDescending(o => o.id).ToList();
                }
            }
            catch (Exception e)
            {
                logger.Error<Exception>(e);
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> Save(Organizer input)
        {
            RestResponse res = new RestResponse();

            APIExtension api = new APIExtension();

            try
            {
                SessionContext context = new SessionContext();
                var userData = context.GetUserData();
                if(input.id == null)
                {
                    string endpoint = string.Format("organizers");
                    string resAPI = await api.PostToken(endpoint, input, userData.token);
                    res = JsonConvert.DeserializeObject<RestResponse>(resAPI);

                    if (res != null && res.isSuccess == true)
                    {
                        var resOrg = JsonConvert.DeserializeObject<Organizer>(res.Data);
                        res.Message = "Save Successfully id: " + resOrg.id;
                    }
                }
                else
                {
                    string endpointupdate = string.Format("organizers/{0}", input.id);
                    string resAPI = await api.PutToken(endpointupdate, input, userData.token);
                    res = JsonConvert.DeserializeObject<RestResponse>(resAPI);
                    if (res.isSuccess == true)
                    {
                        res.Message = "Update Successfully id: " + input.id;
                    }
                }
              

            }
            catch (Exception e)
            {
                logger.Error<Exception>(e);
            }

            return Json(res, JsonRequestBehavior.DenyGet);
        }

        public async Task<JsonResult> Delete(Organizer input)
        {
            RestResponse res = new RestResponse();
            APIExtension api = new APIExtension();
            try
            {
                SessionContext context = new SessionContext();
                var userData = context.GetUserData();

                string endPoint = string.Format("organizers/{0}", input.id);
                string resAPI = await api.DeleteToken(endPoint, userData.token);
                res = JsonConvert.DeserializeObject<RestResponse>(resAPI);
            }
            catch (Exception e)
            {
                logger.Error<Exception>(e);
            }

            return Json(res, JsonRequestBehavior.DenyGet);
        }

    }
}