﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using VOX_WEB.Models;
using VOX_WEB.Utils;

namespace VOX_WEB.Controllers
{
    [Authorize]

    public class SportEventController : Controller
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        // GET: SportEvent
        public ActionResult Index()
        {
            return View();
        }
        public async Task<PartialViewResult> Edit(string id)
        {
            SportEvent res = new SportEvent();
            APIExtension api = new APIExtension();
            try
            {
                SessionContext context = new SessionContext();
                var userData = context.GetUserData();
                if (!String.IsNullOrEmpty(id))
                {
                    string endPoint = string.Format("sport-events/{0}", id);
                    res = await api.Get<SportEvent>(endPoint, userData.token);
                }
                else
                {
                    res.organizer = new Organizer();
                }

            }
            catch (Exception e)
            {
                logger.Error<Exception>(e);
            }

            return PartialView(res);
        }
        public async Task<JsonResult> getSportEventAll()
        {
            SportEventList res = new SportEventList();
            List<SportEventRes> resSports = new List<SportEventRes>();

            APIExtension api = new APIExtension();

            try
            {
                SessionContext context = new SessionContext();
                var userData = context.GetUserData();

                string endPoint = "sport-events?page=0&perPage=5000";
                res = await api.Get<SportEventList>(endPoint, userData.token);
                if (res != null && res.data != null)
                {
                    res.data = res.data.OrderByDescending(o => o.id).ToList();
                    foreach (var resSport in res.data)
                    {
                        SportEventRes model = new SportEventRes();
                        model.eventDate = resSport.eventDate;
                        model.eventName = resSport.eventName;
                        model.eventType = resSport.eventType;
                        model.id = resSport.id ?? 0;
                        model.organizer = resSport.organizer.organizerName;

                        resSports.Add(model);
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error<Exception>(e);
            }

            return Json(resSports, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> Save(SportEvent input)
        {
            RestResponse res = new RestResponse();

            APIExtension api = new APIExtension();

            try
            {
                SessionContext context = new SessionContext();
                var userData = context.GetUserData();
                if (input.id == null)
                {
                    string endpoint = string.Format("sport-events");
                    string resAPI = await api.PostToken(endpoint, input, userData.token);
                    res = JsonConvert.DeserializeObject<RestResponse>(resAPI);

                    if (res != null && res.isSuccess == true)
                    {
                        var resOrg = JsonConvert.DeserializeObject<Organizer>(res.Data);
                        res.Message = "Save Successfully id: " + resOrg.id;
                    }
                }
                else
                {
                    string endpointupdate = string.Format("sport-events/{0}", input.id);
                    string resAPI = await api.PutToken(endpointupdate, input, userData.token);
                    res = JsonConvert.DeserializeObject<RestResponse>(resAPI);
                    if (res.isSuccess == true)
                    {
                        res.Message = "Update Successfully id: " + input.id;
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error<Exception>(e);
            }

            return Json(res, JsonRequestBehavior.DenyGet);
        }

        public async Task<JsonResult> Delete(SportEvent input)
        {
            RestResponse res = new RestResponse();
            APIExtension api = new APIExtension();
            try
            {
                SessionContext context = new SessionContext();
                var userData = context.GetUserData();

                string endPoint = string.Format("sport-events/{0}", input.id);
                string resAPI = await api.DeleteToken(endPoint, userData.token);
                res = JsonConvert.DeserializeObject<RestResponse>(resAPI);
            }
            catch (Exception e)
            {
                logger.Error<Exception>(e);
            }

            return Json(res, JsonRequestBehavior.DenyGet);
        }

    }
}