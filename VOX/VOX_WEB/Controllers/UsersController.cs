﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using VOX_WEB.Models;
using VOX_WEB.Utils;

namespace VOX_WEB.Controllers
{
    [Authorize]
    public class UsersController : Controller
    {
        // GET: Users
        private static Logger logger = LogManager.GetCurrentClassLogger();

        //public ActionResult Index()
        //{
        //    return View();
        //}
        public ActionResult GetUser()
        {
            ViewBag.Version = Guid.NewGuid().ToString().Replace("-", "");
            return View();
        }

        public ActionResult UpdateUser()
        {
            ViewBag.Version = Guid.NewGuid().ToString().Replace("-", "");
            return View();
        }
        public ActionResult DeleteUser()
        {
            ViewBag.Version = Guid.NewGuid().ToString().Replace("-", "");
            return View();
        }
        public ActionResult CreateUser()
        {
            ViewBag.Version = Guid.NewGuid().ToString().Replace("-", "");
            return View();
        }
        public async Task<JsonResult> getUserByID(UserOutput input)
        {
            UserResult res = new UserResult();

            APIExtension api = new APIExtension();

            try
            {
                SessionContext context = new SessionContext();
                var userData = context.GetUserData();

                string endPoint = string.Format("users/{0}", input.id);
                res= await api.Get<UserResult>(endPoint, userData.token);
            }
            catch (Exception e)
            {
                logger.Error<Exception>(e);
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> UpdateUserAPI(UserResult input)
        {
            UserResult res = new UserResult();

            APIExtension api = new APIExtension();

            try
            {
                SessionContext context = new SessionContext();
                var userData = context.GetUserData();

                string endpointupdate = string.Format("users/{0}", input.id);
                string resAPI = await api.PutToken(endpointupdate, input, userData.token);

                string endPoint = string.Format("users/{0}", input.id);
                res = await api.Get<UserResult>(endPoint, userData.token);
            }
            catch (Exception e)
            {
                logger.Error<Exception>(e);
            }

            return Json(res, JsonRequestBehavior.DenyGet);
        }

        public async Task<JsonResult> CreateUserAPI(UserCreateInput input)
        {
            RestResponse res = new RestResponse();

            APIExtension api = new APIExtension();

            try
            {
                SessionContext context = new SessionContext();
                var userData = context.GetUserData();

                string endpoint = string.Format("users");
                string resAPI = await api.PostToken(endpoint, input, userData.token);
                res = JsonConvert.DeserializeObject<RestResponse>(resAPI);

                if (res != null && res.isSuccess==true)
                {
                    var resLogin = JsonConvert.DeserializeObject<Users>(res.Data);
                    res.Message = "Save Successfully id: " + resLogin.id;
                }

            }
            catch (Exception e)
            {
                logger.Error<Exception>(e);
            }

            return Json(res, JsonRequestBehavior.DenyGet);
        }

        public async Task<JsonResult> DeleteUserAPI(UserResult input)
        {
            RestResponse res = new RestResponse();

            APIExtension api = new APIExtension();

            try
            {
                SessionContext context = new SessionContext();
                var userData = context.GetUserData();

                string endPoint = string.Format("users/{0}", input.id);
                string resAPI = await api.DeleteToken(endPoint, userData.token);
                res = JsonConvert.DeserializeObject<RestResponse>(resAPI);



            }
            catch (Exception e)
            {
                logger.Error<Exception>(e);
            }

            return Json(res, JsonRequestBehavior.DenyGet);
        }
    }
}