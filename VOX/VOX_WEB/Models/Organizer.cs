﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VOX_WEB.Models
{
    public class Organizer
    {
        public int? id { get; set; }
        public string organizerName { get; set; }
        public string imageLocation { get; set; }
    }

    public class OrganizerList
    {
        public List<Organizer> data { get; set; }
    }
}