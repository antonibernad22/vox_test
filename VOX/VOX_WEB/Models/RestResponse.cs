﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VOX_WEB.Models
{
    public class RestResponse
    {
        public bool isSuccess { get; set; }
        public int IsSuccessStatusCode { get; set; }
        public string Message { get; set; }
        public DateTime RequestTime { get; set; }
        public string Data { get; set; }
        public string ErrorMessage { get; set; }
    }
    public class RestResponse<T>
    {
        public bool isSuccess { get; set; }
        public int IsSuccessStatusCode { get; set; }
        public string Message { get; set; }
        public DateTime RequestTime { get; set; }
        //public string Data { get; set; }
        private List<T> list;
        public List<T> Data { get { return list; } set { list = value; } }
        public string ErrorMessage { get; set; }
    }
}