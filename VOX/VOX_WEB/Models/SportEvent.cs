﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VOX_WEB.Models
{
    public class SportEvent
    {
        public string eventDate { get; set; }
        public string eventName { get; set; }
        public string eventType { get; set; }
        public int? id { get; set; }
        public int? organizerId { get; set; }

        public Organizer organizer { get; set; }
    }

    public class SportEventRes
    {
        public string eventDate { get; set; }
        public string eventName { get; set; }
        public string eventType { get; set; }
        public int id { get; set; }
        public string organizer { get; set; }
    }

    public class SportEventList
    {
        public List<SportEvent> data { get; set; }
    }
}