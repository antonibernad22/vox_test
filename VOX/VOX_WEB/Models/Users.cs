﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VOX_WEB.Models
{
    public class Users
    {
        public int id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string repeatPassword { get; set; }
    }
    public class UserCreateInput
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string repeatPassword { get; set; }
    }
    public class UserInput
    {
        public string email { get; set; }
        public string password { get; set; }

    }

    public class UserOutput
    {
        public int id { get; set; }
        public string email { get; set; }
        public string token { get; set; }

    }

    public class UserResult
    {
        public int id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
    }

}