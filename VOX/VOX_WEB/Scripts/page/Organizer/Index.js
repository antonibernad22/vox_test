﻿var organizerds = [];


$(document).ready(function () {
    startSpinner('Loading..', 1);

    $.when(getOrganizerAll()).done(function () {
        bindGrid();
        startSpinner('Loading..', 0);
    });
});


function getOrganizerAll() {
    return $.ajax({
        url: urlGetOrganizerAll,
        success: function (result) {
            organizerds = [];
            organizerds = result.data;

            //console.log(JSON.stringify(organizerds));
        },
        error: function (data) {
            alert('Something Went Wrong');
            startSpinner('loading..', 0);
        }
    });
}

function bindGrid() {
    $("#gvList").kendoGrid({
        dataSource: {
            data: organizerds,
            schema: {
                model: {
                    id: "id",
                    fields: {
                        "id": { type: "number" },
                        "organizerName": { type: "string" },
                        "imageLocation": { type: "string" },
                    }
                }
            },
            pageSize: 20
        },

        sortable: true,
        resizable: true,
        reorderable: true,
        // height: 350,
        noRecords: true,
        pageable: {
            pageSizes: [5, 10, 20, 100],
            //change: function (e) { prepareActionGrid(); }
        },
        dataBound: function (e) {
            prepareActionGrid();
        },
        columns: [
            {
                title: "Actions",
                width: "50px",
                exportable: { excel: false },
                template: '<center><a class="btn btn-danger btn-sm deleteData" href="javascript:void(0)" data-id="#=id#"   ><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>&nbsp&nbsp<a class="btn btn-info btn-sm editData" href="javascript:void(0)" data-id="#=id#"  ><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a> </center>'
            },
            { field: "id", title: "id", width: "70px", encoded: false },
            { field: "organizerName", title: "Organizer Name", width: "150px", encoded: false },
            { field: "imageLocation", title: "Image Location", width: "150px", encoded: false },

        ]
    }).data('kendoGrid');
}


function prepareActionGrid() {
    $(".deleteData").on("click", function () {
        var id = $(this).data("id");
        var savedata = {
            "id": id,
        }

        swal({
            type: 'warning',
            title: 'Are you sure?',
            html: 'You want to delete this data',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d9534f'
        }).then(function (isConfirm) {
            if (isConfirm.value == true) {
                startSpinner('Loading..', 1);

                $.ajax({
                    type: "POST",
                    url: urlDelete,
                    data: savedata,
                    success: function (result) {

                        if (result.isSuccess == false) {
                            alert(result.ErrorMessage);
                        }
                        else {
                            $.when(getOrganizerAll()).done(function () {
                                $('#gvList').kendoGrid('destroy').empty();
                                bindGrid();
                            });
                            alert("delete successfully");
                        }
                        startSpinner('loading..', 0);

                        //console.log(result);
                    },
                    error: function (data) {
                        alert('Something Went Wrong');
                        startSpinner('loading..', 0);
                    }
                });
            } else {
                return false;
            }
        });
    });

    $(".editData").on("click", function () {
        var id = $(this).data("id");
        startSpinner('Loading..', 1);
        var link = urlEditForm + "/" + id
        $("#divEdit").load(link, function () {
            document.getElementById('divList').style.display = 'none';
            document.getElementById('divEdit').style.display = '';
            document.getElementById('divBtn').style.display = 'none';

            startSpinner('Loading..', 0);

        });
    });
}

function onAddNewClicked() {
    startSpinner('Loading..', 1);
    $("#divEdit").load(urlEditForm, function () {
        document.getElementById('divList').style.display = 'none';
        document.getElementById('divEdit').style.display = '';
        document.getElementById('divBtn').style.display = 'none';

        startSpinner('Loading..', 0);

    });
}

function onSaveClicked() {
    $("#form2").validate();
    if (!$('#form2').valid()) {
        return false;
    }
    var savedata = {
        "organizerName": $("#organizerName").val(),
        "imageLocation": $("#imageLocation").val(),
        "id": $("#hid").val(),
    }

    swal({
        type: 'warning',
        title: 'Are you sure?',
        html: 'You want to submit this data',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d9534f'
    }).then(function (isConfirm) {
        if (isConfirm.value == true) {
            startSpinner('Loading..', 1);

            $.ajax({
                type: "POST",
                url: urlSave,
                data: savedata,
                success: function (result) {
                    if (result.isSuccess == false) {
                        alert(result.ErrorMessage);
                    }
                    else {
                        document.getElementById('divList').style.display = '';
                        document.getElementById('divEdit').style.display = 'none';
                        document.getElementById('divBtn').style.display = '';
                        $.when(getOrganizerAll()).done(function () {
                            $('#gvList').kendoGrid('destroy').empty();

                            bindGrid();
                        });
                        alert(result.Message);

                    }
                    startSpinner('loading..', 0);

                    console.log(result);
                },
                error: function (data) {
                    alert('Something Went Wrong');
                    startSpinner('loading..', 0);
                }
            });
        } else {
            return false;
        }
    });

    console.log(JSON.stringify(savedata));
}

function onBackClicked() {
    document.getElementById('divList').style.display = '';
    document.getElementById('divEdit').style.display = 'none';
    document.getElementById('divBtn').style.display = '';
}





