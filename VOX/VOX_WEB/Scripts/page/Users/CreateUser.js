﻿

function onCreateClicked() {
    $("#form2").validate();
    if (!$('#form2').valid()) {
        return false;
    }
    var savedata = {
        "firstName": $("#firstName").val(),
        "lastName": $("#lastName").val(),
        "email": $("#email").val(),
        "password": $("#password").val(),
        "repeatPassword": $("#repeatPassword").val(),

    }
    //console.log(JSON.stringify(savedata));
    swal({
        type: 'warning',
        title: 'Are you sure?',
        html: 'You want to create this data',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d9534f'
    }).then(function (isConfirm) {
        if (isConfirm.value == true) {
            startSpinner('Loading..', 1);

            $.ajax({
                type: "POST",
                url: urlCreateUser,
                data: savedata,
                success: function (result) {
                    if (result.isSuccess == false) {
                        alert(result.ErrorMessage);
                    }
                    else {
                        $("#email").val("");
                        $("#firstName").val("");
                        $("#lastName").val("");
                        $("#password").val("");
                        $("#repeatPassword").val("");
                        alert(result.Message);

                    }
                    startSpinner('loading..', 0);

                    console.log(result);
                },
                error: function (data) {
                    alert('Something Went Wrong');
                    startSpinner('loading..', 0);
                }
            });
        } else {
            return false;
        }
    });
}










