﻿

function onViewClicked() {
    
    $("#form1").validate();
    if (!$('#form1').valid()) {
        return false;
    }
    var savedata = {
        "id": $("#id").val(),
    }

    swal({
        type: 'warning',
        title: 'Are you sure?',
        html: 'You want to view this data',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d9534f'
    }).then(function (isConfirm) {
        if (isConfirm.value == true) {
            startSpinner('Loading..', 1);

            $.ajax({
                type: "POST",
                url: urlgetUserByID,
                data: savedata,
                success: function (result) {
                    $("#email").val(result.email);
                    $("#firstName").val(result.firstName);
                    $("#lastName").val(result.lastName);

                    startSpinner('loading..', 0);

                    console.log(result);
                },
                error: function (data) {
                    alert('Something Went Wrong');
                    startSpinner('loading..', 0);
                }
            });
        } else {
            return false;
        }
    });

   
}












