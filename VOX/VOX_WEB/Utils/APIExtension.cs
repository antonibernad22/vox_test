﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using VOX_WEB.Models;

namespace VOX_WEB.Utils
{
    public class APIExtension
    {
        private Logger logger = LogManager.GetCurrentClassLogger();

        static string baseUrl = ConfigurationSettings.AppSettings["baseUrl"];

        public async Task<T> Get<T>(string url, string token = null)
        {
            string resultContentString = "";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                var result = await client.GetAsync(url);
                try
                {
                    resultContentString = await result.Content.ReadAsStringAsync();
                    result.EnsureSuccessStatusCode();

                    T resultContent = JsonConvert.DeserializeObject<T>(resultContentString);
                    return resultContent;
                }
                catch (HttpRequestException e)
                {
                    RestResponse rs = new RestResponse();
                    rs.IsSuccessStatusCode = Convert.ToInt32(result.StatusCode);
                    rs.ErrorMessage = resultContentString;
                    string s = JsonConvert.SerializeObject(rs);
                    T resultContent = JsonConvert.DeserializeObject<T>(s);
                   
                    string msg = Environment.NewLine + "Url :" + baseUrl + url + Environment.NewLine + "Output: " + resultContent + Environment.NewLine + "Message Error: " + e.Message + Environment.NewLine + "Trace: " + e.StackTrace;
                    this.logger.Error(msg);
                    return resultContent;

                }
                catch (Exception e)
                {
                    RestResponse rs = new RestResponse();
                    rs.IsSuccessStatusCode = Convert.ToInt32(result.StatusCode);
                    rs.ErrorMessage = resultContentString;
                    string s = JsonConvert.SerializeObject(rs);
                    T resultContent = JsonConvert.DeserializeObject<T>(s);
                    string msg = Environment.NewLine + "Url :" + baseUrl + url + Environment.NewLine + "Output: " + resultContent + Environment.NewLine + "Message Error: " + e.Message + Environment.NewLine + "Trace: " + e.StackTrace;
                    this.logger.Error(msg);


                    return resultContent;
                }
            }
        }

        public async Task<string> PostToken<T>(string url, T contentValue, string token)
        {
            string resultContentString = "";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var content = new StringContent(JsonConvert.SerializeObject(contentValue), Encoding.UTF8, "application/json");
                var result = await client.PostAsync(url, content);
                try
                {
                    resultContentString = await result.Content.ReadAsStringAsync();

                    result.EnsureSuccessStatusCode();
                    RestResponse rs = new RestResponse();
                    rs.isSuccess = true;
                    rs.IsSuccessStatusCode = Convert.ToInt32(result.StatusCode);
                    rs.ErrorMessage = "";
                    rs.Data = resultContentString;
                    string resultContent = JsonConvert.SerializeObject(rs);
                    return resultContent;
                }
                catch (HttpRequestException e)
                {
                    RestResponse rs = new RestResponse();
                    rs.IsSuccessStatusCode = Convert.ToInt32(result.StatusCode);
                   
                    rs.ErrorMessage = resultContentString;
                    string resultContent = JsonConvert.SerializeObject(rs);
                    string msg = Environment.NewLine + "Url :" + baseUrl + url + Environment.NewLine + "Input: " + JsonConvert.SerializeObject(contentValue) + Environment.NewLine + "Output: " + resultContent + Environment.NewLine + "Message Error: " + e.Message + Environment.NewLine + "Trace: " + e.StackTrace;
                    this.logger.Error(msg);

                    return resultContent;
                }
                catch (Exception e)
                {
                    RestResponse rs = new RestResponse();
                    rs.IsSuccessStatusCode = Convert.ToInt32(result.StatusCode);
                    rs.ErrorMessage = resultContentString;
                    string resultContent = JsonConvert.SerializeObject(rs);
                    string msg = Environment.NewLine + "Url :" + baseUrl + url + Environment.NewLine + "Input: " + JsonConvert.SerializeObject(contentValue) + Environment.NewLine + "Output: " + resultContent + Environment.NewLine + "Message Error: " + e.Message + Environment.NewLine + "Trace: " + e.StackTrace;
                    this.logger.Error(msg);
                    return resultContent;
                }
            }
        }

        public async Task<string> PutToken<T>(string url, T contentValue, string token)
        {
            string resultContentString = "";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var content = new StringContent(JsonConvert.SerializeObject(contentValue), Encoding.UTF8, "application/json");

                var result = await client.PutAsync(url, content);
                try
                {
                    resultContentString = await result.Content.ReadAsStringAsync();
                    result.EnsureSuccessStatusCode();

                    RestResponse rs = new RestResponse();
                    rs.isSuccess = true;
                    rs.IsSuccessStatusCode = Convert.ToInt32(result.StatusCode);
                    rs.ErrorMessage = "";
                    rs.Data = resultContentString;
                    string resultContent = JsonConvert.SerializeObject(rs);
                    return resultContent;
                }
                catch (HttpRequestException e)
                {
                    RestResponse rs = new RestResponse();
                    rs.IsSuccessStatusCode = Convert.ToInt32(result.StatusCode);
                    rs.ErrorMessage = resultContentString;
                    string resultContent = JsonConvert.SerializeObject(rs);
                    string msg = Environment.NewLine + "Url :" + baseUrl + url + Environment.NewLine + "Input: " + JsonConvert.SerializeObject(contentValue) + Environment.NewLine + "Output: " + resultContent + Environment.NewLine + "Message Error: " + e.Message + Environment.NewLine + "Trace: " + e.StackTrace;
                    this.logger.Error(msg);

                    return resultContent;
                }
                catch (Exception e)
                {
                    RestResponse rs = new RestResponse();
                    rs.IsSuccessStatusCode = Convert.ToInt32(result.StatusCode);
                    rs.ErrorMessage = resultContentString;
                    string resultContent = JsonConvert.SerializeObject(rs);
                    string msg = Environment.NewLine + "Url :" + baseUrl + url + Environment.NewLine + "Input: " + JsonConvert.SerializeObject(contentValue) + Environment.NewLine + "Output: " + resultContent + Environment.NewLine + "Message Error: " + e.Message + Environment.NewLine + "Trace: " + e.StackTrace;
                    this.logger.Error(msg);
                    return resultContent;
                }
            }
        }

        public async Task<string> DeleteToken(string url, string token)
        {
            string resultContentString = "";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                
                var result = await client.DeleteAsync(url);
                try
                {
                    resultContentString = await result.Content.ReadAsStringAsync();
                    result.EnsureSuccessStatusCode();

                    RestResponse rs = new RestResponse();
                    rs.isSuccess = true;
                    rs.IsSuccessStatusCode = Convert.ToInt32(result.StatusCode);
                    rs.ErrorMessage = "";
                    rs.Data = resultContentString;
                    string resultContent = JsonConvert.SerializeObject(rs);
                    return resultContent;
                }
                catch (HttpRequestException e)
                {
                    RestResponse rs = new RestResponse();
                    rs.IsSuccessStatusCode = Convert.ToInt32(result.StatusCode);
                    rs.ErrorMessage = resultContentString;
                    string resultContent = JsonConvert.SerializeObject(rs);
                    string msg = Environment.NewLine + "Url :" + baseUrl + url + Environment.NewLine + "Output: " + resultContent + Environment.NewLine + "Message Error: " + e.Message + Environment.NewLine + "Trace: " + e.StackTrace;
                    this.logger.Error(msg);

                    return resultContent;
                }
                catch (Exception e)
                {
                    RestResponse rs = new RestResponse();
                    rs.IsSuccessStatusCode = Convert.ToInt32(result.StatusCode);
                    rs.ErrorMessage = resultContentString;
                    string resultContent = JsonConvert.SerializeObject(rs);
                    string msg = Environment.NewLine + "Url :" + baseUrl + url + Environment.NewLine + "Output: " + resultContent + Environment.NewLine + "Message Error: " + e.Message + Environment.NewLine + "Trace: " + e.StackTrace;
                    this.logger.Error(msg);
                    return resultContent;
                }
            }
        }


        public async Task<string> Post<T>(string url, T contentValue)
        {
            string resultContentString = "";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                var content = new StringContent(JsonConvert.SerializeObject(contentValue), Encoding.UTF8, "application/json");
                var result = await client.PostAsync(url, content);
                try
                {
                    result.EnsureSuccessStatusCode();
                    resultContentString = await result.Content.ReadAsStringAsync();
                    RestResponse rs = new RestResponse();
                    rs.isSuccess = true;

                    rs.IsSuccessStatusCode = Convert.ToInt32(result.StatusCode);
                    rs.ErrorMessage = "";
                    rs.Data = resultContentString;
                    string resultContent = JsonConvert.SerializeObject(rs);
                    return resultContent;
                }
                catch (HttpRequestException e)
                {
                    RestResponse rs = new RestResponse();
                    rs.IsSuccessStatusCode = Convert.ToInt32(result.StatusCode);
                    rs.ErrorMessage = resultContentString;
                    string resultContent = JsonConvert.SerializeObject(rs);
                    string msg = Environment.NewLine + "Url :" + baseUrl + url + Environment.NewLine + "Input: " + JsonConvert.SerializeObject(contentValue) + Environment.NewLine + "Output: " + resultContent + Environment.NewLine + "Message Error: " + e.Message + Environment.NewLine + "Trace: " + e.StackTrace;
                    this.logger.Error(msg);
                    return resultContent;
                }
                catch (Exception e)
                {
                    RestResponse rs = new RestResponse();
                    rs.IsSuccessStatusCode = Convert.ToInt32(result.StatusCode);
                    rs.ErrorMessage = resultContentString;
                    string resultContent = JsonConvert.SerializeObject(rs);
                    string msg = Environment.NewLine + "Url :" + baseUrl + url + Environment.NewLine + "Input: " + JsonConvert.SerializeObject(contentValue) + Environment.NewLine + "Output: " + resultContent + Environment.NewLine + "Message Error: " + e.Message + Environment.NewLine + "Trace: " + e.StackTrace;
                    this.logger.Error(msg);
                    return resultContent;
                }
            }

        }
    }
}